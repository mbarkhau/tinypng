TinyPNG API
-----------

Python module and command line tool for [tinypng.com][1]

Shrink PNG files. Advanced lossy compression for PNG images
that preserves full alpha transparency.

Note: This project is not affiliated with [tinypng.com][1] or [Voormedia B.V.][2]

Important: You require an API key which you may obtain from
[tinypng.com/developers][3].


Besides specifying keys via command line arguments you can:

1. Set the environment variable TINYPNG_API_KEY
2. Create a .tinypng.keys file in your home directory
3. Create a tinypng.keys file in the current directory


[1]: https://tinypng.com
[2]: http://voormedia.com/
[3]: https://tinypng.com/developers